class Meetup {
    organise() {
        console.log('Organising Meetup');
    }
    static getMeetupFounderDetails() {
        console.log("Meetup Founder Details");
    }
}


class TechMeet extends Meetup {
    organise() {
        //super.organise();
        console.log('Organising TechMeet');
        super.organise();
    }
    static getMeetupFounderDetails() {
        console.log("TechMeet Founder Details");
        super.getMeetupFounderDetails();
    }
}
let js = new TechMeet();
js.organise();
/* Output: 
Organising TechMeet
Organising Meetup */
TechMeet.getMeetupFounderDetails();
/* Output: 
TechMeet Founder Details
Meetup Founder Details */