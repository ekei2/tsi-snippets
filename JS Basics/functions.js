const square = function(side)
{

  return side * side
}

console.log(square);

const exponent = function(base, exponent){
    return Math.pow(base, exponent)
}

console.log(exponent(5,2));


const voidFunction = function(){
    console.log('i dont really return anything :D')
}

console.log(voidFunction());

const hummus = function(factor) {
    const ingredient = function(amount, unit, name) {let ingredientAmount = amount * factor;
        if (ingredientAmount > 1) {
          unit += "s";
        }
        console.log(`${ingredientAmount} ${unit} ${name}`);
       };
       ingredient(1, "can", "chickpeas");
       ingredient(0.25, "cup", "tahini");
       ingredient(0.25, "cup", "lemon juice");
       ingredient(1, "clove", "garlic");
       ingredient(2, "tablespoon", "olive oil");
       ingredient(0.5, "teaspoon", "cumin");
};


const power = (base, exponent) => {
    let result = 1;
    for (let count = 0; count < exponent; count++) {
      result *= base;
    }
    return result;
  };


const square1 = (x) => { return x * x; };
const square2 = x => x * x;


let myInt = Number('334');

console.log(myInt);




function bake (cakeType){
  alert (`We just baked a ${cakeType} cake`);
}

bake(prompt('please enter what type of cake you want'))
