let varName1 = [1,2,3,4]
let varName2 = ["one", "two", "three" , "four"]
let varString = 'hello world'

console.log(varName1[1])
console.log(varName2[3])
console.log(varString['length'])

console.log(Math.max(1,5,10,23,44,2,8,4));

console.log('Hello Ernest'.toUpperCase())

let pushTest = [1,2,5,8]
pushTest.push(100); // adds an item to thhe end of the array
console.log(pushTest)
pushTest.pop(); //removes the last item in the array
console.log(pushTest)
pushTest.shift() //removes the first item in the array
console.log(pushTest)
pushTest.splice(1,1); // this will go to index 1 and remove 1 item
console.log(pushTest);


let obj= {
    property1: false,
    property2: ["work", "touched tree", "pizza", "running"],
    myFunction : function(){
         return 'this function just ran';
    }
 };

 delete obj.property1
 

 console.log(Object.keys({x: 0, y: 0, z: 2}));

 let objectA = {a: 1, b: 2};
Object.assign(objectA, {b: 3, c: 4});
console.log(objectA);

//REFERENCES POINTING AT THE SAME OBJECT

let object1 = {value: 10};
let object2 = object1;
let object3 = {value: 10};
console.log(object1 == object2);
//true
console.log(object1 == object3);
//false

object1.value = 15

console.log(object2);


//Mutability
const score = {visitors: 0, home: 0};
// This is okay
score.visitors = 1;
// This isn't allowed
score = {visitors: 1, home: 1};