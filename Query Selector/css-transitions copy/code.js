let show = function(elem){
	let getHeight = function (){
		elem.style.display= 'block';
		let height = elem.scrollHeight + 'px';
		elem.style.display = '';
		return height;
	}

	let height = getHeight();
	elem.classList.add('is-visible')
	elem.style.height = height;
	
	
	window.setTimeout(function () {
		elem.style.height = '';
	}, 350);
}

let hide = function(elem){
	elem.style.height = elem.scrollHeight + 'px';

	window.setTimeout(function () {
		elem.style.height = '0';
	}, 1);

	window.setTimeout(function () {
		elem.classList.remove('is-visible');
	}, 350);
}

const toggle = function(elem, timing){
	if(elem.classList.contains('is-visible')){
		hide(elem);
		return
	}
	show(elem);
}


let toggleButton = document.querySelector('.toggle');

toggleButton.addEventListener('click', (e)=>{
	e.preventDefault();
	elem = document.querySelector('.toggle-content')
	toggle(elem);
})