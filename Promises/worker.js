self.addEventListener('message',(e)=>{
    let list = []
    let evenGen = function(n){
        let i = 0;
        let going = true;
        do{
            if(i%2 === 0){
                list.push(i);
            }
            if(list.length === e.data){
                going = false;
            }
            i++
        }while(going);
        
        return list
    }
    self.postMessage(evenGen(e.data))

})